var monOr = "";
var monDes = "";
var cantidad = "";
var subtotal = "";
var totalComision = "";
var totalPagar = "";
var totDat = 0;
var num = /^[+-]?\d+(\.\d+)?$/g;

var opt_1 = "";
var opt_2 = "";
var opt_3 = "";
var opt_4 = "";

var calcular = document.getElementById('calcular');
var registrar = document.getElementById('registrar');
var borrarRegistro = document.getElementById('borrarRegistro');

function Change() {

    opt_1 = new Array("Moneda Destino", "Pesos Mexicanos", "Dólar Canadiense", "Euros");
    opt_2 = new Array("Moneda Destino", "Dólar Estadounidense", "Dólar Canadiense", "Euros");
    opt_3 = new Array("Moneda Destino", "Dólar Estadounidense", "Pesos Mexicanos", "Euros");
    opt_4 = new Array("Moneda Destino", "Dólar Estadounidense", "Pesos Mexicanos", "Dólar Canadiense");

    var op;
    op = document.opciones.monOr[document.opciones.monOr.selectedIndex].value;

    if (op != 0) {
        Opt = eval("opt_" + op);
        numOpt = Opt.length;
        document.opciones.monDes.length = numOpt;
        for (i = 0; i < numOpt; i++) {
            document.opciones.monDes.options[i].value = Opt[i];
            document.opciones.monDes.options[i].text = Opt[i];
        }
    } else {
        document.opciones.monDes.length = 1;
        document.opciones.monDes.options[0].value = "-";
        document.opciones.monDes.options[0].text = "-";
    }
    document.opciones.monDes.options[0].selected = true;
}

function Calcular(monOr, monDes, cantidad, subtotal, totalComision, totalPagar) {

    monOr = document.getElementById('monOr');
    monDes = document.getElementById('monDes');
    cantidad = document.getElementById('cantidad').value;
    subtotal = document.getElementById('subtotal').value;
    totalComision = document.getElementById('totalComision').value;
    totalPagar = document.getElementById('totalPagar').value;


    // 1 - Dolar estadounidense
    // 2 - Peso mexicano
    // 3 - Dolar canadiense
    // 4 - 4o

    monOr.value = document.opciones.monOr[document.opciones.monOr.selectedIndex].value;
    monDes.value = document.opciones.monDes[document.opciones.monDes.selectedIndex].value;

    console.log(monOr.value);
    console.log(monDes.value);

    if (monOr.value == "1" && monDes.value == "Pesos Mexicanos") {
        subtotal = cantidad * 19.85;
        document.getElementById("subtotal").value = subtotal.toFixed(2);
        totalComision = subtotal * 0.03;
        document.getElementById("totalComision").value = totalComision.toFixed(2);
        totalPagar = subtotal + totalComision;
        document.getElementById("totalPagar").value = totalPagar.toFixed(2);
    }
    if (monOr.value == "1" && monDes.value == "Dólar Canadiense") {
        subtotal = cantidad * 1.35;
        document.getElementById("subtotal").value = subtotal.toFixed(2);
        totalComision = subtotal * 0.03;
        document.getElementById("totalComision").value = totalComision.toFixed(2);
        totalPagar = subtotal + totalComision;
        document.getElementById("totalPagar").value = totalPagar.toFixed(2);

    }
    if (monOr.value == "1" && monDes.value == "Euros") {
        subtotal = cantidad * 0.99;
        document.getElementById("subtotal").value = subtotal.toFixed(2);
        totalComision = subtotal * 0.03;
        document.getElementById("totalComision").value = totalComision.toFixed(2);
        totalPagar = subtotal + totalComision;
        document.getElementById("totalPagar").value = totalPagar.toFixed(2);

    }
    if (monOr.value == "2" && monDes.value == "Dólar Estadounidense") {
        subtotal = cantidad * 0.050;
        document.getElementById("subtotal").value = subtotal.toFixed(2);
        totalComision = subtotal * 0.03;
        document.getElementById("totalComision").value = totalComision.toFixed(2);
        totalPagar = subtotal + totalComision;
        document.getElementById("totalPagar").value = totalPagar.toFixed(2);

    }
    if (monOr.value == "2" && monDes.value == "Dólar Canadiense") {
        subtotal = cantidad * 0.068;
        document.getElementById("subtotal").value = subtotal.toFixed(2);
        totalComision = subtotal * 0.03;
        document.getElementById("totalComision").value = totalComision.toFixed(2);
        totalPagar = subtotal + totalComision;
        document.getElementById("totalPagar").value = totalPagar.toFixed(2);

    }
    if (monOr.value == "2" && monDes.value == "Euros") {
        subtotal = cantidad * 0.051;
        document.getElementById("subtotal").value = subtotal.toFixed(2);
        totalComision = subtotal * 0.03;
        document.getElementById("totalComision").value = totalComision.toFixed(2);
        totalPagar = subtotal + totalComision;
        document.getElementById("totalPagar").value = totalPagar.toFixed(2);

    }
    if (monOr.value == "3" && monDes.value == "Pesos Mexicanos") {
        subtotal = cantidad * 14.65;
        document.getElementById("subtotal").value = subtotal.toFixed(2);
        totalComision = subtotal * 0.03;
        document.getElementById("totalComision").value = totalComision.toFixed(2);
        totalPagar = subtotal + totalComision;
        document.getElementById("totalPagar").value = totalPagar.toFixed(2);

    }
    if (monOr.value == "3" && monDes.value == "Dólar Estadounidense") {
        subtotal = cantidad * 0.74;
        document.getElementById("subtotal").value = subtotal.toFixed(2);
        totalComision = subtotal * 0.03;
        document.getElementById("totalComision").value = totalComision.toFixed(2);
        totalPagar = subtotal + totalComision;
        document.getElementById("totalPagar").value = totalPagar.toFixed(2);

    }
    if (monOr.value == "3" && monDes.value == "Euros") {
        subtotal = cantidad * 0.74;
        document.getElementById("subtotal").value = subtotal.toFixed(2);
        totalComision = subtotal * 0.03;
        document.getElementById("totalComision").value = totalComision.toFixed(2);
        totalPagar = subtotal + totalComision;
        document.getElementById("totalPagar").value = totalPagar.toFixed(2);

    }
    if (monOr.value == "4" && monDes.value == "Pesos Mexicanos") {
        subtotal = cantidad * 19.85;
        document.getElementById("subtotal").value = subtotal.toFixed(2);
        totalComision = subtotal * 0.03;
        document.getElementById("totalComision").value = totalComision.toFixed(2);
        totalPagar = subtotal + totalComision;
        document.getElementById("totalPagar").value = totalPagar.toFixed(2);

    }
    if (monOr.value == "4" && monDes.value == "Dólar Estadounidense") {
        subtotal = cantidad * 0.99;
        document.getElementById("subtotal").value = subtotal.toFixed(2);
        totalComision = subtotal * 0.03;
        document.getElementById("totalComision").value = totalComision.toFixed(2);
        totalPagar = subtotal + totalComision;
        document.getElementById("totalPagar").value = totalPagar.toFixed(2);

    }
    if (monOr.value == "4" && monDes.value == "Dólar Canadiense") {
        subtotal = cantidad * 1.35;
        document.getElementById("subtotal").value = subtotal.toFixed(2);
        totalComision = subtotal * 0.03;
        document.getElementById("totalComision").value = "$ " + totalComision.toFixed(2);
        totalPagar = subtotal + totalComision;
        document.getElementById("totalPagar").value = "$ " + totalPagar.toFixed(2);

    }

    console.log(monOr.value);
    console.log(monDes.value);
}

function Registrar() {
    let tot = document.getElementById('totalPagar').value;
    console.log(tot);
    tot = tot.match(num);
    console.log(tot);
    cantidad = document.getElementById("cantidad").value;
    monOr = String(document.opciones.monOr[document.opciones.monOr.selectedIndex].text);
    monDes = String(document.opciones.monDes[document.opciones.monDes.selectedIndex].value) ;
    if (monDes == "Moneda destino" || monOr == 0 || cantidad < 1) {
        alert("Escogió un dato mayor a 0");
    }
    else{
        var subtotalRe = document.getElementById("subtotal").value;
        var totalComisionRe = document.getElementById("totalComision").value;
        var totalPagarRe = document.getElementById("totalPagar").value;
        
        totDat = parseFloat(tot) + totDat;
        console.log(totDat);
        
        const lista = document.querySelector("#lista1");
        const lista2 = document.createElement("lista2");
        lista2.textContent = " - Cantidad: " + cantidad + " | Moneda origen: " + monOr + " | Moneda destino: " + monDes + " | Subtotal: " + subtotalRe + " | Comisión: " + totalComisionRe + " | Total a pagar: " + totalPagarRe;
        lista.appendChild(lista2);
        document.getElementById("totDat").innerHTML = "$ " + totDat.toFixed(2);
    }
}


function limpiar() {
    document.getElementById("monOr").value = "0";
    document.getElementById("monDes").value = "Moneda Destino";
    document.getElementById("cantidad").value = "";
    document.getElementById("subtotal").value = "";
    document.getElementById("totalComision").value = "";
    document.getElementById("totalPagar").value = "";
}

function Borrar() {
    const lista = document.querySelector("#lista1");
    lista.remove();
    alert("Se han borrado los registros!");
    location.reload();
}
